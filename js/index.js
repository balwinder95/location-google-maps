function initialize() {             //to initialize a map
    var mapOptions = {
        center: new google.maps.LatLng(24, 80),
        zoom: 5
    };
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);  //create map object
    var input = (document.getElementById('inputBox'));
    var autocomplete = new google.maps.places.Autocomplete(input);  //create an autocomplete object
    var infowindow = new google.maps.InfoWindow();      //create an info window object
  
    var marker = new google.maps.Marker({       //create marker object and set values
        map: map,
        draggable:true,
        animation: google.maps.Animation.DROP
    });

    google.maps.event.addListener(autocomplete, 'place_changed', function() {     //function executed when input is changed
        infowindow.close();         //close info window and marker
        marker.setVisible(false);

        var place = autocomplete.getPlace();    //get place from autocomplete
        map.setCenter(place.geometry.location);
        map.setZoom(10);

        marker.setPosition(place.geometry.location);    //set marker to the place in input
        marker.setVisible(true);

        infowindow.setContent('<div><strong>' + place.name + '</strong><br></div>' + place.geometry.location.lat() + ", " + place.geometry.location.lng()); //display address in info window
        infowindow.open(map, marker);
    });       

    google.maps.event.addListener(marker,'dragstart',function(){        //to close info window when marker drag is started
        infowindow.close();
    });

    google.maps.event.addListener(marker,'dragend',function(a){         //function executed when marker is dropped
        var latitude=a.latLng.lat();    //get latitude
        var longitude=a.latLng.lng();   //get longitude
        var latlang=new google.maps.LatLng(latitude,longitude);
        var geocoder=new google.maps.Geocoder();    //create Geocoder object
        geocoder.geocode({'latLng': latlang}, function(results, status) {       //function to get address from coordinates
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    infowindow.setContent('<div><strong>' + results[1].formatted_address + '</strong></div><br>' + latitude +', '+longitude);  //display address in info window
                    infowindow.open(map, marker);
                    input.value=results[1].formatted_address;  //display address in input field
                }
            }
        });
    });
}
google.maps.event.addDomListener(window, 'load', initialize);   //to load map when page is loaded